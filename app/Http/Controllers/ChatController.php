<?php

namespace App\Http\Controllers;

use App\Models\Chat;
use App\Models\ChatMessage;
use App\Models\ChatReply;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\ChatGrant;
use Twilio\Rest\Client;

class ChatController extends Controller
{
    public function adminHome()
    {
        $allTodayChats = Chat::whereDate('created_at', Carbon::today())->get();
        return view('home', compact('allTodayChats'));
    }

    /*public function adminChat($id)
    {
        $authUser = Auth::user()->id;
        // // $otherUser = User::find(explode('-', $ids)[1]);
        $clients = Chat::where('id', '<>', $authUser->id)->get();

        $twilio = new Client(env('TWILIO_AUTH_SID'), env('TWILIO_AUTH_TOKEN'));

        // Fetch channel or create a new one if it doesn't exist
        try {
            $channel = $twilio->chat->v2->services(env('TWILIO_SERVICE_SID'))
                ->channels($ids)
                ->fetch();
        } catch (\Twilio\Exceptions\RestException $e) {
            $channel = $twilio->chat->v2->services(env('TWILIO_SERVICE_SID'))
                ->channels
                ->create([
                    'uniqueName' => $ids,
                    'type' => 'private',
                ]);
        }

        // // Add first user to the channel
        // try {
        //     $twilio->chat->v2->services(env('TWILIO_SERVICE_SID'))
        //         ->channels($ids)
        //         ->members($authUser->email)
        //         ->fetch();
        // } catch (\Twilio\Exceptions\RestException $e) {
        //     $member = $twilio->chat->v2->services(env('TWILIO_SERVICE_SID'))
        //         ->channels($ids)
        //         ->members
        //         ->create($authUser->email);
        // }

        // // Add second user to the channel
        // try {
        //     $twilio->chat->v2->services(env('TWILIO_SERVICE_SID'))
        //         ->channels($ids)
        //         ->members($otherUser->email)
        //         ->fetch();
        // } catch (\Twilio\Exceptions\RestException $e) {
        //     $twilio->chat->v2->services(env('TWILIO_SERVICE_SID'))
        //         ->channels($ids)
        //         ->members
        //         ->create($otherUser->email);
        // }

        // return view('messages.chat', compact('users', 'otherUser'));
    }
    */

    public function chatClient(Request $request)
    {
        $save = ChatMessage::create([
            'chat_user_id' => $request->from,
            'admin_id' => $request->to,
            'pharmacy_id' => $request->pharmacyId,
            'message' => $request->msg
        ]);
        if ($save) {
            return response()->json(['status' => true]);
        } else {
            return response()->json(['status' => false]);
        }
    }

    public function chatAdmin(Request $request)
    {
        $save = ChatReply::create([
            'admin_id' => $request->from,
            'chat_user_id' => $request->to,
            'pharmacy_id' => $request->pharmacyId,
            'message' => $request->msg
        ]);

        if ($save) {
            return response()->json(['status' => true]);
        } else {
            return response()->json(['status' => false]);
        }
    }

    public function chat(Request $request)
    {
        $findChatUser = Chat::where('email', $request->email)->first();
        if ($findChatUser) {
            $chatUser = $findChatUser;
        } else {
            $chatUser = Chat::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
            ]);
        }
        return response()->json(['user' => $chatUser]);

        /*
        $twilio = new Client(env('TWILIO_AUTH_SID'), env('TWILIO_AUTH_TOKEN'));

        // Fetch channel or create a new one if it doesn't exist
        try {
            $channel = $twilio->chat->v2->services(env('TWILIO_SERVICE_SID'))
                ->channels($chatUser->id)
                ->fetch();
        } catch (\Twilio\Exceptions\RestException $e) {
            $channel = $twilio->chat->v2->services(env('TWILIO_SERVICE_SID'))
                ->channels
                ->create([
                    'uniqueName' => $chatUser->id,
                    'type' => 'private',
                ]);
        }

        // Add first user to the channel
        try {
            $twilio->chat->v2->services(env('TWILIO_SERVICE_SID'))
                ->channels($chatUser->id)
                ->members($chatUser->email)
                ->fetch();
        } catch (\Twilio\Exceptions\RestException $e) {
            $member = $twilio->chat->v2->services(env('TWILIO_SERVICE_SID'))
                ->channels($chatUser->id)
                ->members
                ->create($chatUser->email);
        }
        
        return response()->json(['user' => $chatUser]);
        */
    }

    public function sirAdminChat()
    {
        $allActiveClients = DB::table('chat')->where('chat_date', Carbon::now()->toDateString())->where('chat_active', 1)->get();
        return view('new-home', compact('allActiveClients'));
        // return view('new-home-three');
    }

    public function chatSirTemp(Request $request)
    {
        $findChatUser = DB::table('chat')->where('chat_user_email', $request->email)->first();
        $allOldChats = null;
        if ($findChatUser) {
            $chatUserId = $findChatUser->chat_id;
            DB::table('chat')->where('chat_id', $chatUserId)->update([
                'chat_date' => Carbon::now()->toDateString(),
                'chat_user_full_name' => $request->name,
                'chat_user_phone' => $request->phone,
                'chat_active' => 1,
                'chat_in_progress' => 1,
            ]);
            $allOldChats = $this->getOldChats($chatUserId);
        } else {
            $chatUserId = DB::table('chat')->insertGetId([
                'chat_date' => Carbon::now()->toDateString(),
                'chat_user_full_name' => $request->name,
                'chat_user_email' => $request->email,
                'chat_user_phone' => $request->phone,
                'chat_active' => 1,
                'chat_in_progress' => 1,
                'chat_unread_message_count' => 0,
                'chat_initiated_at' => Carbon::now(),
            ]);
            $allOldChats = null;
        }
        return response()->json(['userId' => $chatUserId, 'allOldChats' => $allOldChats]);
    }

    public function sirChatClient(Request $request)
    {
        if ($request->userType == 'client') {
            $save = $this->clientChatInfoSave($request);
        }

        if ($request->userType == 'admin') {
            $save = $this->adminChatInfoSave($request);
        }


        if ($save) {
            return response()->json(['status' => true]);
        } else {
            return response()->json(['status' => false]);
        }
    }

    public function clientChatInfoSave($request)
    {
        $msgNumber = DB::table('chat_conversation')->select('message_number')->where('chat_id', $request->from)->orderBy('id', 'desc')->first();
        if (is_null($msgNumber)) {
            $msgNumber = 1;
        } else {
            $msgNumber = $msgNumber->message_number + 1;
        }

        $save = DB::table('chat_conversation')->insert([
            'chat_id' => $request->from,
            'admin_id' => $request->to,
            'pharmacy_id' => $request->pharmacyId,
            'message_from' => $request->userType,
            'message_number' => $msgNumber,
            'message_from_user' => $request->msg,
            'message_from_operator' => '',
            'message_datetime' => Carbon::now()
        ]);
        return $save;
    }

    public function adminChatInfoSave($request)
    {
        $msgNumber = DB::table('chat_conversation')->select('message_number')->where('chat_id', $request->to)->orderBy('id', 'desc')->first();

        if (is_null($msgNumber)) {
            $msgNumber = 1;
        } else {
            $msgNumber = $msgNumber->message_number + 1;
        }

        $save = DB::table('chat_conversation')->insert([
            'chat_id' => $request->to,
            'admin_id' => $request->from,
            'pharmacy_id' => $request->pharmacyId,
            'message_from' => $request->userType,
            'message_number' => $msgNumber,
            'message_from_user' => '',
            'message_from_operator' => $request->msg,
            'message_datetime' => Carbon::now()
        ]);
        return $save;
    }

    public function getOldChats($id)
    {
        $allOldChats = DB::table('chat_conversation')->select('message_from', 'message_number', 'message_from_user', 'message_from_operator', 'message_attachment')->where('chat_id', $id)->get();
        return $allOldChats;
    }

    public function chatFileUpload(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'file' => 'required|mimes:doc,docx,txt,jpg,jpeg,gif,png,pdf'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()->first()]);
        }

        $currentDate = date('Y_m_d_H_i');
        $orginalName = $request->file('file')->getClientOriginalName();
        $file = $request->file('file');
        $file_name = $currentDate . '_' . $orginalName;
        $file->move(storage_path('/app/public/files') . '/', $currentDate . '_' . $orginalName);

        $msgNumber = DB::table('chat_conversation')->select('message_number')->where('chat_id', $request->chat_id)->orderBy('id', 'desc')->first();

        if (is_null($msgNumber)) {
            $msgNumber = 1;
        } else {
            $msgNumber = $msgNumber->message_number + 1;
        }

        if ($request->userType == 'client') {
            $save = DB::table('chat_conversation')->insert([
                'chat_id' => $request->chat_id,
                'admin_id' => $request->to,
                'pharmacy_id' => $request->pharmacyId,
                'message_from' => $request->userType,
                'message_number' => $msgNumber,
                'message_from_user' => '',
                'message_from_operator' => '',
                'message_attachment' => $file_name,
                'message_datetime' => Carbon::now()
            ]);
        }

        if ($request->userType == 'admin') {
            $save = DB::table('chat_conversation')->insert([
                'chat_id' => $request->chat_id,
                'admin_id' => $request->from,
                'pharmacy_id' => $request->pharmacyId,
                'message_from' => $request->userType,
                'message_number' => $msgNumber,
                'message_from_user' => '',
                'message_from_operator' => '',
                'message_attachment' => $file_name,
                'message_datetime' => Carbon::now()
            ]);
        }
        if ($save) {
            return response()->json(['status' => true, 'fileNmae' => $file_name]);
        } else {
            return response()->json(['status' => false]);
        }
    }

    public function chatStop(Request $request)
    {
        $data = DB::table('chat')->where('chat_id', $request->chat_id)->update([
            'chat_active' => 0,
            'chat_in_progress' => 0,
        ]);
        if ($data) {
            return response()->json(['status' => true]);
        }
    }

    public function sirFindClient(Request $request)
    {
        $clientInfo = DB::table('chat')->where('chat_id', $request->chat_id)->first();
        $clientChats = DB::table('chat_conversation')->where('chat_id', $request->chat_id)->get();
        return response()->json(['status' => true, 'clientInfo' => $clientInfo, 'clientChats' => $clientChats]);
    }
    /*
    public function generateToken(Request $request)
    {
        $token = new AccessToken(
            env('TWILIO_AUTH_SID'),
            env('TWILIO_API_SID'),
            env('TWILIO_API_SECRET'),
            3600,
            $request->email
        );

        $chatGrant = new ChatGrant();
        $chatGrant->setServiceSid(env('TWILIO_SERVICE_SID'));
        $token->addGrant($chatGrant);

        return response()->json([
            'email' => $request->email,
            'token' => $token->toJWT()
        ]);
    }
    */

    /*
    public function newChat(Request $request)
    {
        ChatMessage::create([
            'chat_user_id' => $request->userId,
            'message' => $request->message,
        ]);
    }
    */

    // -> 01761122115

    public function getAllChat(Request $request)
    {
        $allChats = array();
        $allActiveClients = DB::table('chat')->where('chat_date', Carbon::now()->toDateString())->where('chat_active', 1)->get();
        for ($i = 0; $i < count($allActiveClients); $i++) {
            $allChats[$i] = getAllChatsById($allActiveClients[$i]->chat_id);
        }

        return response()->json(['status' => true, 'allClients' => $allActiveClients, 'allChats' => $allChats]);
    }
}
