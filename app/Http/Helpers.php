<?php

use Illuminate\Support\Facades\DB;

function getAllChatsById($id)
{
    $allChats = DB::table('chat_conversation')->where('chat_id', $id)->get();
    // dd($allChats);
    return $allChats;
}
