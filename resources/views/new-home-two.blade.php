@extends('layouts.app-2')

@section('content')
<?php
$pharmacyAdminId = 1;
$pharmcyId = 1;
?>
<div class="container">
    <div id="frame">
        <div id="sidepanel">
            <div id="contacts">
                <ul style="padding-left: 0px;">
                    @forelse($allActiveClients as $client)
                    <li class="tablinks contact @if($loop->first) active @endif" id="{{ $client->chat_id }}">
                        <div class="wrap">
                            <span class="contact-status online"></span>
                            <img src="http://emilcarlsson.se/assets/louislitt.png" alt="" />
                            <div class="meta">
                                <p class="name">{{ $client->chat_user_full_name }}</p>
                            </div>
                        </div>
                    </li>
                    @empty
                    <li class="tablinks contact">
                        <div class="wrap">
                            <div class="meta">
                                <p class="name">No Chat Yet</p>
                            </div>
                        </div>
                    </li>
                    @endforelse
                </ul>
            </div>
        </div>
        <div class="ajApp">
            @forelse($allActiveClients as $client)
            <div class="tabcontent content @if($loop->first) active @else inactive @endif" id="{{ $client->chat_id }}">
                <div class="messages">
                    <ul>
                        <?php
                        $allChats = getAllChatsById($client->chat_id);
                        for ($i = 0; $i < count($allChats); $i++) {
                            if (!is_null($allChats[$i])) {
                                if ($allChats[$i]->message_from == 'client') {
                        ?>
                                    <li class="sent">
                                        <img src="http://emilcarlsson.se/assets/mikeross.png" alt="" />
                                        <?php if (!empty($allChats[$i]->message_from_user)) { ?>
                                            <p>{{ $allChats[$i]->message_from_user }}</p>
                                        <?php } ?>
                                        <?php if (!empty($allChats[$i]->message_attachment)) { ?>
                                            <p><a href="{{ asset('/storage/files/'.$allChats[$i]->message_attachment) }}" download="">{{ $allChats[$i]->message_attachment }}</a></p>
                                        <?php } ?>
                                    </li>
                                <?php
                                } else if ($allChats[$i]->message_from == 'admin') {
                                ?>
                                    <li class="replies">
                                        <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                                        <?php if (!empty($allChats[$i]->message_from_operator)) { ?>
                                            <p>{{ $allChats[$i]->message_from_operator }}</p>
                                        <?php } ?>
                                        <?php if (!empty($allChats[$i]->message_attachment)) { ?>
                                            <p><a href="{{ asset('/storage/files/'.$allChats[$i]->message_attachment) }}" download="">{{ $allChats[$i]->message_attachment }}</a></p>
                                        <?php } ?>
                                    </li>
                        <?php }
                            }
                        } ?>
                    </ul>
                </div>
                <div class="message-input">
                    <div class="wrap">
                        <input class="client_id" type="hidden" value="{{ $client->chat_id }}" />
                        <input type="text" class="write_msg" placeholder="Write your message..." />
                        <input type="file" class="upload_file" name="file" style="display: none">
                        <i class="fa fa-paperclip attachment file_atch_btn" aria-hidden="true"></i>
                        <button class="msg_send_btn"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
            @empty
            <div class="tabcontent content active">
                <div class="messages">
                    <ul>
                        <li class="sent">
                            <p>No Chat yet</p>
                    </ul>
                </div>
            </div>
            @endforelse
        </div>
    </div>
    <audio id="notificationAudio" src="{{ asset('sound/notification.mp3') }}" type="audio/mp3">
</div>
@endsection

@section('chat-scripts')
<script>
    var btn_ents = document.getElementsByClassName('msg_send_btn');
    var btn_attch = document.getElementsByClassName('file_atch_btn');
    var msgs = document.getElementsByClassName('write_msg');
    var upload_file = document.getElementsByClassName('upload_file');
    var fileName = document.getElementsByName('file');
    var client_ids = document.getElementsByClassName("client_id");
    var conn = new WebSocket("ws://localhost:5000");
    conn.onopen = function(e) {
        console.log("Connection established!");
        var msg = {
            command: 'register',
            userId: "{{ $pharmacyAdminId }}"
        }
        msg = JSON.stringify(msg);
        console.log('init connection')
        conn.send(msg);
    };
    conn.onmessage = function(e) {
        // if(e.data)
        console.log(e.data);
        var data = JSON.parse(e.data);
        console.log(data.from);
        // var audio = document.getElementById("notificationAudio");
        // audio.play();
        showMessages("client", data.from, data);
    };

    for (var i = 0; i < btn_ents.length; i++) {
        (function(index) {
            btn_ents[index].addEventListener('click', function() {
                if (msgs[index].value != "") {
                    message = msgs[index].value;
                    clientId = client_ids[index].value;
                    var msg = {
                        to: clientId,
                        from: "{{ $pharmacyAdminId }}",
                        pharmacyId: "{{ $pharmcyId }}",
                        userType: 'admin',
                        command: 'message',
                        msg: message
                    };
                    console.log(msg)
                    msg = JSON.stringify(msg);
                    conn.send(msg);
                    showMessages("admin", index, msg);
                    msgs[index].value = "";
                }
            });

            btn_attch[index].addEventListener('click', function() {
                upload_file[index].click();
                clientId = client_ids[index].value;
                fileName[index].onchange = function() {
                    var fileName = this.files[0].name;
                    var msg = {
                        to: clientId,
                        from: "{{ $pharmacyAdminId }}",
                        pharmacyId: "{{ $pharmcyId }}",
                        userType: 'admin',
                        command: 'message',
                    };
                    uploadFile(fileName, this.files[0], msg, index);
                };
            });
        })(i);
    }

    function uploadFile(fileName, imgDDD, msg, index) {
        console.log('called');
        var form_data = new FormData();
        form_data.append('_token', '{{csrf_token()}}');
        form_data.append('file', imgDDD, fileName);
        form_data.append('chat_id', msg.to);
        form_data.append('to', msg.to);
        form_data.append('from', msg.from);
        form_data.append('pharmacyId', '{{ $pharmcyId}}');
        form_data.append('userType', 'admin');

        $.ajax({
            type: 'POST',
            url: '{{ route("chat-file-upload") }}',
            processData: false,
            contentType: false,
            data: form_data,
            success: function(response) {
                console.log(response);
                if (response.status == false) {
                    alert(response.error)
                }
                if (response.status == true) {
                    console.log('uploaded')
                    msg.msg = response.fileNmae;
                    msg.file = true;
                    console.log(msg)
                    msg = JSON.stringify(msg);
                    conn.send(msg);
                    showMessages("admin", index, msg)
                }
            },
            error: function(errors) {
                console.log(errors)
            }
        })
    }

    function showMessages(who, chatIndex, data) {
        if (who == "admin") {
            data = JSON.parse(data);
            console.log(data);
            if (data.file == true) {
                $('.messages ul').append('<li class="replies"><img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" /> <p><a href="' + '{{asset("/storage/files")}}' + '/' + data.msg + '" download>' + data.msg + '</a></p> </li>');
            } else {
                $('.messages ul').append('<li class="replies"><img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" /> <p>' + data.msg + '</p> </li>');
            }
            saveDate(data);
        } else {
            area_content = document.getElementById(data.from);
            if (area_content != null) {
                if (data.file == true) {
                    $('.messages ul').append('<li class="sent"><img src="http://emilcarlsson.se/assets/mikeross.png" alt="" /> <p><a href="' + '{{asset("/storage/files")}}' + '/' + data.msg + '" download>' + data.msg + '</a></p> </li>');
                } else {
                    $('.messages ul').append('<li class="sent"><img src="http://emilcarlsson.se/assets/mikeross.png" alt="" /> <p>' + data.msg + '</p> </li>');
                }
                // var audio = document.getElementById("notificationAudio");
                // audio.play();
            } else {
                console.log(data);
                $.ajax({
                    method: 'POST',
                    url: '{{ route("find-client-by-id") }}',
                    data: {
                        chat_id: data.from,
                        _token: '{{csrf_token()}}'
                    },
                    success: function(response) {
                        console.log(response)
                        if (response.status == true) {
                            $('#contacts ul').append('<li class="tablinks contact" id="' + response.clientInfo.chat_id + '"><div class="wrap"><span class="contact-status online"></span> <img src = "http://emilcarlsson.se/assets/louislitt.png" alt = ""/> <div class="meta"> <p class="name">' + response.clientInfo.chat_user_full_name + '</p> </div></div> </li>');
                            var container = $('<div class="tabcontent content inactive" id="' + response.clientInfo.chat_id + '">');

                            $.each(response.clientChats, function(key, value) {
                                if (value.message_from == 'client') {
                                    if (value.message_from_user != null && value.message_from_user != "") {
                                        container.append('<div class="messages"><ul><li class="sent"><img src="http://emilcarlsson.se/assets/mikeross.png" alt="" /> <p>' + value.message_from_user + '</p> </li></ul></div>');
                                    }
                                }
                            })

                            $('.ajApp').append(container);
                        }
                    }
                })
                // window.location.reload()
            }
        }
    }

    function saveDate(data) {
        $.ajax({
            method: 'POST',
            url: '{{ route("sir-chat-client") }}',
            data: {
                userType: data.userType,
                from: data.from,
                msg: data.msg,
                pharmacyId: data.pharmacyId,
                to: data.to,
                _token: '{{csrf_token()}}'
            },
            success: function(response) {
                console.log(response)
            }
        });
    }
</script>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        var tabLink = $('#contacts').on('click', '.contact', function() {
            console.log(this.id)
            tabLink.removeClass('active');
            $(this).addClass('active');
            changeChatBoxContent(this.id);
        });

        function changeChatBoxContent(Id) {
            var tabContent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabContent.length; i++) {
                if (tabContent[i].id == Id) {
                    tabContent[i].classList.add('active');
                    tabContent[i].classList.remove('inactive');
                } else {
                    tabContent[i].classList.remove('active');
                    tabContent[i].classList.add('inactive');
                }
            }
        }


    });
</script>
@endsection