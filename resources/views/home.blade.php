@extends('layouts.app')

@section('content')
<?php
$pharmacyAdminId = 1;
$pharmcyId = 1;
?>
<div class="container">
    <div class="row">
        <div class="col-3">
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <a class="nav-link active" id="user-tab-27" data-toggle="pill" href="#user-pills-27" role="tab" aria-controls="v-pills-27" aria-selected="true">User 27</a>
                <a class="nav-link" id="user-tab-28" data-toggle="pill" href="#user-pills-28" role="tab" aria-controls="v-pills-28" aria-selected="false">User 28</a>
            </div>
        </div>
        <div class="col-9">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="user-pills-27" role="tabpanel" aria-labelledby="user-tab-27">
                    <div class="mesgs">
                        <div class="msg_history" id="27">

                        </div>
                        <div class="type_msg">
                            <div class="input_msg_write">
                                <input class="client_id" type="hidden" value="27" />
                                <input type="text" class="write_msg" placeholder="Type a message" />
                                <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="user-pills-28" role="tabpanel" aria-labelledby="user-tab-28">
                    <div class="mesgs">
                        <div class="msg_history" id="28">

                        </div>
                        <div class="type_msg">
                            <div class="input_msg_write">
                                <input class="client_id" type="hidden" value="28" />
                                <input type="text" class="write_msg" placeholder="Type a message" />
                                <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('chat-scripts')
<script>
    var btn_ents = document.getElementsByClassName('msg_send_btn');
    var msgs = document.getElementsByClassName('write_msg');
    var client_ids = document.getElementsByClassName("client_id");
    var conn = new WebSocket("ws://localhost:5000");
    conn.onopen = function(e) {
        console.log("Connection established!");
        var msg = {
            command: 'register',
            userId: "{{ $pharmacyAdminId }}"
        }
        msg = JSON.stringify(msg);
        console.log('init connection')
        conn.send(msg);
    };
    conn.onmessage = function(e) {
        // if(e.data)
        console.log(e.data);
        var data = JSON.parse(e.data);
        console.log(data.from);
        showMessages("client", data.from, data);
    };

    for (var i = 0; i < btn_ents.length; i++) {
        (function(index) {
            btn_ents[index].addEventListener('click', function() {
                if (msgs[index].value != "") {
                    message = msgs[index].value;
                    clientId = client_ids[index].value;
                    var msg = {
                        to: clientId,
                        from: 1,
                        pharmacyId: "{{ $pharmcyId }}",
                        userType: 'admin',
                        command: 'message',
                        msg: message
                    };
                    console.log(msg)
                    msg = JSON.stringify(msg);
                    conn.send(msg);
                    showMessages("admin", index, msg);
                    msgs[index].value = "";
                }
            })
        })(i);
    }

    /*
    <div class="incoming_msg">
        <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
        <div class="received_msg">
            <div class="received_withd_msg">
                <p>Test which is a new approach to have all
                    solutions</p>
                <span class="time_date"> 11:01 AM | June 9</span>
            </div>
        </div>
    </div>
    <div class="outgoing_msg">
        <div class="sent_msg">
            <p>Test which is a new approach to have all
                solutions</p>
            <span class="time_date"> 11:01 AM | June 9</span>
        </div>
    </div>
    */
    function showMessages(who, chatIndex, data) {
        if (who == "admin") {
            data = JSON.parse(data);
            // document.querySelectorAll('.msg_history').forEach((x) => {
            //     // console.log(x)
            //     var div = document.createElement("div");
            //     div.setAttribute("class", 'outgoing_msg');
            //     var div_sent_msg = document.createElement("div");
            //     div_sent_msg.setAttribute("class", 'sent_msg');
            //     var p = document.createElement("p");
            //     p.textContent = data.msg;
            //     div_sent_msg.appendChild(p);
            //     div.appendChild(div_sent_msg)
            //     x.appendChild(div);
            // });
            area_content = document.getElementById(data.to);
            var div = document.createElement("div");
            div.setAttribute("class", 'outgoing_msg');
            var div_sent_msg = document.createElement("div");
            div_sent_msg.setAttribute("class", 'sent_msg');
            var p = document.createElement("p");
            p.textContent = data.msg;
            div_sent_msg.appendChild(p);
            div.appendChild(div_sent_msg)
            area_content.appendChild(div);
        } else {
            console.log('called')
            // document.querySelectorAll('.msg_history').forEach((x) => {
            //     var div = document.createElement("div");
            //     div.setAttribute("class", 'incoming_msg');
            //     var div_incoming_msg_img = document.createElement("div");
            //     div_incoming_msg_img.setAttribute("class", 'incoming_msg_img');
            //     var img = document.createElement("img");
            //     img.setAttribute("src", 'https://ptetutorials.com/images/user-profile.png');
            //     div_incoming_msg_img.appendChild(img);
            //     div.appendChild(div_incoming_msg_img)
            //     var div_received_msg = document.createElement("div");
            //     div_received_msg.setAttribute("class", 'received_msg');

            //     var div_received_withd_msg = document.createElement("div");
            //     div_received_withd_msg.setAttribute("class", 'received_withd_msg');
            //     var p = document.createElement("p");
            //     p.textContent = data.msg;
            //     div_received_withd_msg.appendChild(p);
            //     div_received_msg.appendChild(div_received_withd_msg);
            //     div.appendChild(div_received_msg);
            //     x.appendChild(div);
            // });
            area_content = document.getElementById(data.from);
            var div = document.createElement("div");
            div.setAttribute("class", 'incoming_msg');
            var div_incoming_msg_img = document.createElement("div");
            div_incoming_msg_img.setAttribute("class", 'incoming_msg_img');
            var img = document.createElement("img");
            img.setAttribute("src", 'https://ptetutorials.com/images/user-profile.png');
            div_incoming_msg_img.appendChild(img);
            div.appendChild(div_incoming_msg_img)
            var div_received_msg = document.createElement("div");
            div_received_msg.setAttribute("class", 'received_msg');

            var div_received_withd_msg = document.createElement("div");
            div_received_withd_msg.setAttribute("class", 'received_withd_msg');
            var p = document.createElement("p");
            p.textContent = data.msg;
            div_received_withd_msg.appendChild(p);
            div_received_msg.appendChild(div_received_withd_msg);
            div.appendChild(div_received_msg);
            area_content.appendChild(div);
        }
    }
</script>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {

    })
</script>
@endsection