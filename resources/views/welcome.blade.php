<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Styles -->

</head>

<body class="antialiased">
    <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center sm:pt-0">
        @if (Route::has('login'))
        <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
            @auth
            <a href="{{ url('/dashboard') }}" class="text-sm text-gray-700 underline">Dashboard</a>
            @else
            <a href="{{ route('login') }}" class="text-sm text-gray-700 underline">Login</a>

            @if (Route::has('register'))
            <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 underline">Register</a>
            @endif
            @endauth
        </div>
        @endif
        <audio id="notificationAudio" src="{{ asset('sound/notification.mp3') }}" type="audio/mp3">
            <?php
            $pharmacyAdminId = 1;
            $pharmcyId = 1;
            ?>
    </div>


    <script src="{{ asset('js/chat_variables.js') }}"></script>
    <script src="{{ asset('js/jquery-1.11.3.min.js') }}"></script>

    <script type="text/javascript">
        $("head").append(external_css);
        $("head").append(internal_css);
        $("body").prepend(chat_body);

        var conn = new WebSocket("ws://localhost:5000");

        conn.onopen = function(e) {
            console.log("Connection established!");
        }
        conn.onmessage = function(e) {
            console.log(e.data);
            showMessages(e.data);
            var audio = document.getElementById("notificationAudio");
            audio.play();
        };
        var userId = null;

        $(document).ready(function() {
            $("#chat_message").prop('disabled', true);
            $(".fab_field").hide();
            $("#end_chat").hide();
        });

        hideChat(0);

        $('#prime').click(function() {
            toggleFab();
        });

        //Toggle chat and links
        function toggleFab() {
            $('.prime').toggleClass('zmdi-comment-outline');
            $('.prime').toggleClass('zmdi-close');
            $('.prime').toggleClass('is-active');
            $('.prime').toggleClass('is-visible');
            $('#prime').toggleClass('is-float');
            $('.chat').toggleClass('is-visible');
            $('.fab').toggleClass('is-visible');
        }

        $('#chat_first_screen').click(function(e) {
            if ($("#user_name").val() == "" || $("#user_email").val() == "") {
                alert("Pls enter your name and email");
            } else {
                var userName = $("#user_name").val();
                var userEmail = $("#user_email").val();
                var userPhone = $("#user_phone").val();
                chatRegister(userName, userEmail, userPhone);
                $(".fab_field").show();
                $("#chat_message").prop('disabled', false);
                hideChat(1);
            }
        });


        $('#user_name, #user_email, #user_phone').on('keyup', function(e) {
            if (e.keyCode == 13) {
                if ($("#user_name").val() == "" || $("#user_email").val() == "") {
                    alert("Pls enter your name and email");
                } else {
                    var userName = $("#user_name").val();
                    var userEmail = $("#user_email").val();
                    var userPhone = $("#user_phone").val();
                    // Chat Register
                    chatRegister(userName, userEmail, userPhone);
                    $(".fab_field").show();
                    $("#chat_message").prop('disabled', false);
                    hideChat(1);
                }
            }
        });

        function chatRegister(name, email, phone) {
            $.ajax({
                url: "{{ route('chat-sir-temp') }}",
                method: 'POST',
                data: {
                    name: name,
                    email: email,
                    phone: phone,
                    _token: '{{csrf_token()}}'
                },
                success: function(response) {
                    console.log(response)
                    userId = response.userId;
                    // Call to register client
                    registerClient(userId);
                    $("#end_chat").show();
                    if (response.allOldChats != null) {
                        $.each(response.allOldChats, function(key, value) {
                            if (value.message_from == 'client') {
                                if (value.message_from_user != "" && value.message_from_user != null) {
                                    $("#chat_converse").append('<span class="chat_msg_item chat_msg_item_user">' + value.message_from_user + '</span>');
                                }
                                if (value.message_attachment != "" && value.message_attachment != null) {
                                    $("#chat_converse").append('<span class="chat_msg_item chat_msg_item_user"><a href="' + '{{asset("/storage/files")}}' + '/' + value.message_attachment + '" download>' + value.message_attachment + '</a></span>');
                                }
                            }
                            if (value.message_from == 'admin') {
                                if (value.message_from_operator != "" && value.message_from_operator != null) {
                                    $("#chat_converse").append('<span class="chat_msg_item chat_msg_item_admin"><div class="chat_avatar"><img src="image/admin_avatar.png"/></div>' + value.message_from_operator + '</span>');
                                }
                                if (value.message_attachment != "" && value.message_attachment != null) {
                                    $("#chat_converse").append('<span class="chat_msg_item chat_msg_item_admin"><div class="chat_avatar"><img src="image/admin_avatar.png"/></div><a href="' + '{{asset("/storage/files")}}' + '/' + value.message_attachment + '" download>' + value.message_attachment + '</a></span>');
                                }
                            }
                        });
                    }
                }
            })
        }

        function registerClient(id) {
            var msg = {
                command: 'register',
                userId: id
            }
            msg = JSON.stringify(msg);
            conn.send(msg);
        }

        function showMessages(data) {
            // audioElement.play();
            var data = JSON.parse(data);
            if (data.file == true) {
                $("#chat_converse").append('<span class="chat_msg_item chat_msg_item_admin"><div class="chat_avatar"><img src="image/admin_avatar.png"/></div><a href="' + '{{asset("/storage/files")}}' + '/' + data.msg + '" download>' + data.msg + '</a></span>');
            } else {
                $("#chat_converse").append('<span class="chat_msg_item chat_msg_item_admin"><div class="chat_avatar"><img src="image/admin_avatar.png"/></div>' + data.msg + '</span>');
            }
        }

        $('#chat_fullscreen_loader').click(function(e) {
            $('.fullscreen').toggleClass('zmdi-window-maximize');
            $('.fullscreen').toggleClass('zmdi-window-restore');
            $('.chat').toggleClass('chat_fullscreen');
            $('.fab').toggleClass('is-hide');
            $('.header_img').toggleClass('change_img');
            $('.img_container').toggleClass('change_img');
            $('.chat_header').toggleClass('chat_header2');
            $('.fab_field').toggleClass('fab_field2');
            $('.chat_converse').toggleClass('chat_converse2');
        });

        function hideChat(hide) {
            switch (hide) {
                case 0:
                    $('#chat_converse').css('display', 'none');
                    $('#chat_body').css('display', 'none');
                    $('#chat_form').css('display', 'none');
                    $('.chat_login').css('display', 'block');
                    $('.chat_fullscreen_loader').css('display', 'none');
                    $('#chat_fullscreen').css('display', 'none');
                    break;
                case 1:
                    $('#chat_converse').css('display', 'block');
                    $('#chat_body').css('display', 'none');
                    $('#chat_form').css('display', 'none');
                    $('.chat_login').css('display', 'none');
                    $('.chat_fullscreen_loader').css('display', 'block');
                    break;
            }
        }

        $('#fab_send').click(function(e) {
            if ($("#attachment").val() == '1') {
                send_file();
            } else {
                var message = $("#chat_message").val();
                if (message == "") {
                    alert("write your message pls");
                } else {
                    send_message(message);
                }
            }
        });

        $('#chat_message').on('keyup', function(e) {
            if (e.keyCode == 13) {
                if ($("#attachment").val() == '1') {
                    send_file();
                } else {
                    var message = $("#chat_message").val();
                    if (message == "") {
                        alert("write your message pls");
                    } else {
                        send_message(message);
                    }
                }
            }
        });

        function send_message(message) {
            var msg = {
                to: "{{ $pharmacyAdminId}}",
                from: userId,
                pharmacyId: "{{ $pharmcyId }}",
                userType: 'client',
                command: 'message',
                msg: message
            };
            console.log(msg)
            $.ajax({
                method: 'POST',
                url: '{{ route("sir-chat-client") }}',
                data: {
                    userType: 'client',
                    from: userId,
                    msg: message,
                    pharmacyId: "{{ $pharmcyId }}",
                    to: "{{ $pharmacyAdminId}}",
                    _token: '{{csrf_token()}}'
                },
                success: function(response) {
                    console.log(response)
                }
            });
            msg = JSON.stringify(msg);
            conn.send(msg);
            $("#chat_converse").append('<span class="chat_msg_item chat_msg_item_user">' + message + '</span>');
            $("#chat_message").val("");
        }

        function send_file() {
            var file_data = $('#file').prop('files')[0];
            var form_data = new FormData();
            form_data.append('_token', '{{csrf_token()}}');
            form_data.append('file', file_data);
            form_data.append('chat_id', userId);
            form_data.append('to', '{{ $pharmacyAdminId}}');
            form_data.append('pharmacyId', '{{ $pharmcyId}}');
            form_data.append('userType', 'client');

            $.ajax({
                type: 'POST',
                url: '{{ route("chat-file-upload") }}',
                processData: false,
                contentType: false,
                data: form_data,
                beforeSend: function() {
                    $("#chat_converse").append('<span id="loading" class="chat_msg_item chat_msg_item_user">Uploading file...<img src="image/uploading.gif"/></span>');
                },
                success: function(response) {
                    console.log(response);
                    if (response.status == false) {
                        alert(response.error)
                    }
                    if (response.status == true) {
                        console.log('uploaded')
                        $("#chat_converse").append('<span class="chat_msg_item chat_msg_item_user"><a href="' + '{{asset("/storage/files")}}' + '/' + response.fileNmae + '" download>' + response.fileNmae + '</a></span>');
                        var msg = {
                            to: "{{ $pharmacyAdminId}}",
                            from: userId,
                            pharmacyId: "{{ $pharmcyId }}",
                            userType: 'client',
                            command: 'message',
                            msg: response.fileNmae,
                            file: true
                        };
                        console.log(msg)
                        msg = JSON.stringify(msg);
                        conn.send(msg);
                    }
                    $('span').remove('#loading');
                    $("#chat_message").val("");
                    $('#file').val('');
                    $("#attachment").val(0);
                },
                error: function(errors) {
                    alert('Try Again');
                    $('span').remove('#loading');
                    $("#chat_message").val("");
                    $('#file').val('');
                    $("#attachment").val(0);
                }
            })
        }

        $(".attach_file").click(function() {
            $("#file").click();
            return false;
        });

        $('input[type="file"]').change(function(e) {
            var fileName = e.target.files[0].name;
            $("#attachment").val(1);
            $("#chat_message").val(fileName);
        });

        // End Chat
        $("#end_chat").click(function() {
            if (confirm('Are you sure to end chat?')) {
                $.ajax({
                    type: 'POST',
                    url: '{{ route("chat-stop") }}',
                    data: {
                        chat_id: userId,
                        _token: '{{csrf_token()}}'
                    },
                    success: function(response) {
                        console.log(response)
                        if (response.status == true) {
                            // conn.close();
                            $("#user_name").val("");
                            $("#user_email").val("");
                            $("#user_phone").val("");
                            $(".fab_field").hide();
                            hideChat(0);
                        }
                    }
                });
            } else {
                // Do nothing!
            }
        });
    </script>
</body>

</html>