@extends('layouts.app')

@section('content')
<?php
$pharmacyAdminId = 1;
$pharmcyId = 1;
?>
<div class="container">
    <div class="row">
        <div class="col-3">
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                @forelse($allActiveClients as $client)
                <a class="nav-link @if($loop->first) active @endif" id="user-tab-{{ $client->chat_id }}" data-toggle="pill" href="#user-pills-{{ $client->chat_id }}" role="tab" aria-controls="v-pills-{{ $client->chat_id }}" aria-selected="true">{{ $client->chat_user_full_name }}</a>
                @empty
                <a>No User Found</a>
                @endforelse
            </div>
        </div>
        <div class="col-9">
            <div class="tab-content" id="v-pills-tabContent">
                @forelse($allActiveClients as $client)
                <div class="tab-pane fade @if($loop->first) show active @endif" data-id="{{ $client->chat_id }}" id="user-pills-{{ $client->chat_id }}" role="tabpanel" aria-labelledby="user-tab-{{ $client->chat_id }}">
                    <div class="mesgs">
                        <div class="msg_history" id="{{ $client->chat_id }}">
                            <?php
                            $allChats = getAllChatsById($client->chat_id);
                            for ($i = 0; $i < count($allChats); $i++) {
                                if (!is_null($allChats[$i])) {
                                    if ($allChats[$i]->message_from == 'client') {
                            ?>
                                        <div class="incoming_msg">
                                            <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                                            <div class="received_msg">
                                                <div class="received_withd_msg">
                                                    <?php if (!empty($allChats[$i]->message_from_user)) { ?>
                                                        <p>{{ $allChats[$i]->message_from_user }}</p>
                                                    <?php } ?>
                                                    <?php if (!empty($allChats[$i]->message_attachment)) { ?>
                                                        <a href="{{ asset('/storage/files/'.$allChats[$i]->message_attachment) }}" download="">{{ $allChats[$i]->message_attachment }}</a>
                                                    <?php } ?>
                                                    <span class="time_date"> {{$allChats[$i]->message_datetime}} </span>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                    } else if ($allChats[$i]->message_from == 'admin') {
                                    ?>
                                        <div class="outgoing_msg">
                                            <div class="sent_msg">
                                                <?php if (!empty($allChats[$i]->message_from_operator)) { ?>
                                                    <p>{{ $allChats[$i]->message_from_operator }}</p>
                                                <?php } ?>
                                                <?php if (!empty($allChats[$i]->message_attachment)) { ?>
                                                    <a href="{{ asset('/storage/files/'.$allChats[$i]->message_attachment) }}" download="">{{ $allChats[$i]->message_attachment }}</a>
                                                <?php } ?>
                                                <span class="time_date">{{$allChats[$i]->message_datetime}} </span>
                                            </div>
                                        </div>
                            <?php
                                    }
                                }
                            }
                            ?>
                        </div>
                        <div class="type_msg">
                            <div class="input_msg_write">
                                <input class="client_id" type="hidden" value="{{ $client->chat_id }}" />
                                <input type="text" class="write_msg" placeholder="Type a message" />
                                <input type="file" class="upload_file" name="file" style="display: none">
                                <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                                <button class="file_atch_btn" type="button"><i class="fa fa-plus-square" aria-hidden="true"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <audio id="notificationAudio" src="{{ asset('sound/notification.mp3') }}" type="audio/mp3">
</div>
@endsection

@section('chat-scripts')
<script>
    var btn_ents = document.getElementsByClassName('msg_send_btn');
    var btn_attch = document.getElementsByClassName('file_atch_btn');
    var msgs = document.getElementsByClassName('write_msg');
    var upload_file = document.getElementsByClassName('upload_file');
    var fileName = document.getElementsByName('file');
    var client_ids = document.getElementsByClassName("client_id");
    var conn = new WebSocket("ws://localhost:5000");
    conn.onopen = function(e) {
        console.log("Connection established!");
        var msg = {
            command: 'register',
            userId: "{{ $pharmacyAdminId }}"
        }
        msg = JSON.stringify(msg);
        console.log('init connection')
        conn.send(msg);
    };
    conn.onmessage = function(e) {
        // if(e.data)
        console.log(e.data);
        var data = JSON.parse(e.data);
        console.log(data.from);
        showMessages("client", data.from, data);
    };
    // old submit
    for (var i = 0; i < btn_ents.length; i++) {
        (function(index) {
            btn_ents[index].addEventListener('click', function() {
                if (msgs[index].value != "") {
                    message = msgs[index].value;
                    clientId = client_ids[index].value;
                    var msg = {
                        to: clientId,
                        from: "{{ $pharmacyAdminId }}",
                        pharmacyId: "{{ $pharmcyId }}",
                        userType: 'admin',
                        command: 'message',
                        msg: message
                    };
                    console.log(msg)
                    msg = JSON.stringify(msg);
                    conn.send(msg);
                    showMessages("admin", index, msg);
                    msgs[index].value = "";
                }
            });

            btn_attch[index].addEventListener('click', function() {
                upload_file[index].click();
                clientId = client_ids[index].value;
                fileName[index].onchange = function() {
                    var fileName = this.files[0].name;
                    var msg = {
                        to: clientId,
                        from: "{{ $pharmacyAdminId }}",
                        pharmacyId: "{{ $pharmcyId }}",
                        userType: 'admin',
                        command: 'message',
                    };
                    uploadFile(fileName, this.files[0], msg, index);
                };
            });
        })(i);
    }

    // $('.input_msg_write').on('click', 'msg_send_btn', function() {
    //     console.log('click')
    // })

    function uploadFile(fileName, imgDDD, msg, index) {
        console.log('called');
        var form_data = new FormData();
        form_data.append('_token', '{{csrf_token()}}');
        form_data.append('file', imgDDD, fileName);
        form_data.append('chat_id', msg.to);
        form_data.append('to', msg.to);
        form_data.append('from', msg.from);
        form_data.append('pharmacyId', '{{ $pharmcyId}}');
        form_data.append('userType', 'admin');

        $.ajax({
            type: 'POST',
            url: '{{ route("chat-file-upload") }}',
            processData: false,
            contentType: false,
            data: form_data,
            success: function(response) {
                console.log(response);
                if (response.status == false) {
                    alert(response.error)
                }
                if (response.status == true) {
                    console.log('uploaded')
                    msg.msg = response.fileNmae;
                    msg.file = true;
                    console.log(msg)
                    msg = JSON.stringify(msg);
                    conn.send(msg);
                    showMessages("admin", index, msg)
                }
            },
            error: function(errors) {
                console.log(errors)
            }
        })
    }

    function showMessages(who, chatIndex, data) {
        if (who == "admin") {
            data = JSON.parse(data);
            console.log(data);
            area_content = document.getElementById(data.to);
            var div = document.createElement("div");
            div.setAttribute("class", 'outgoing_msg');
            var div_sent_msg = document.createElement("div");
            div_sent_msg.setAttribute("class", 'sent_msg');
            if (data.file == true) {
                var a = document.createElement('a');
                a.setAttribute('href', '{{ asset("/storage/files")}}' + '/' + data.msg);
                a.setAttribute('download', '');
                a.innerText = data.msg;
                div_sent_msg.appendChild(a);
            } else {
                var p = document.createElement("p");
                p.textContent = data.msg;
                div_sent_msg.appendChild(p);
            }
            div.appendChild(div_sent_msg)
            area_content.appendChild(div);
            saveDate(data);
        } else {
            area_content = document.getElementById(data.from);
            if (area_content != null) {
                var div = document.createElement("div");
                div.setAttribute("class", 'incoming_msg');
                var div_incoming_msg_img = document.createElement("div");
                div_incoming_msg_img.setAttribute("class", 'incoming_msg_img');
                var img = document.createElement("img");
                img.setAttribute("src", 'https://ptetutorials.com/images/user-profile.png');
                div_incoming_msg_img.appendChild(img);
                div.appendChild(div_incoming_msg_img)
                var div_received_msg = document.createElement("div");
                div_received_msg.setAttribute("class", 'received_msg');
                var div_received_withd_msg = document.createElement("div");
                div_received_withd_msg.setAttribute("class", 'received_withd_msg');
                if (data.file == true) {
                    var a = document.createElement('a');
                    a.setAttribute('href', '{{ asset("/storage/files")}}' + '/' + data.msg);
                    a.setAttribute('download', '');
                    a.innerText = data.msg;
                    div_received_withd_msg.appendChild(a);
                } else {
                    var p = document.createElement("p");
                    p.textContent = data.msg;
                    div_received_withd_msg.appendChild(p);
                }
                div_received_msg.appendChild(div_received_withd_msg);
                div.appendChild(div_received_msg);
                area_content.appendChild(div);
                // var audio = document.getElementById("notificationAudio");
                // audio.play();
            } else {
                console.log(data);
                $.ajax({
                    method: 'POST',
                    url: '{{ route("find-client-by-id") }}',
                    data: {
                        chat_id: data.from,
                        _token: '{{csrf_token()}}'
                    },
                    success: function(response) {
                        console.log(response)
                        if (response.status == true) {
                            $('#v-pills-tab').append('<a class="nav-link" id="user-tab-' + response.clientInfo.chat_id + '" data-toggle="pill" href="#user-pills-' + response.clientInfo.chat_id + '" role="tab" aria-controls="v-pills-' + response.clientInfo.chat_id + '" aria-selected="true"> ' + response.clientInfo.chat_user_full_name + ' </a>');
                            $('#v-pills-tabContent').append('<div class="tab-pane fade" data-id="' + response.clientInfo.chat_id + '" id="user-pills-' + response.clientInfo.chat_id + '" role="tabpanel" aria-labelledby="user-tab-' + response.clientInfo.chat_id + '">');
                            var containerParent = $('<div class="mesgs">');
                            var container = $('<div class="msg_history" id="' + response.clientInfo.chat_id + '">');

                            $.each(response.clientChats, function(key, value) {
                                if (value.message_from == 'client') {
                                    if (value.message_from_user != null && value.message_from_user != "") {
                                        container.append('<div class="incoming_msg"> <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"/> </div> <div class="received_msg"><div class="received_withd_msg"><p>' + value.message_from_user + '</p><span class="time_date"> ' + value.message_datetime + ' </span></div></div></div>');
                                    }
                                    if (value.message_attachment != null && value.message_attachment != "") {
                                        container.append('<div class="incoming_msg"> <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"/> </div> <div class="received_msg"><div class="received_withd_msg"><a href="' + '{{asset("/storage/files")}}' + '/' + value.message_attachment + '" download>' + value.message_attachment + '</a><span class="time_date"> ' + value.message_datetime + ' </span></div></div></div>');
                                    }
                                }
                                if (value.message_from == 'admin') {
                                    if (value.message_from_operator != null && value.message_from_operator != "") {
                                        container.append('<div class="outgoing_msg"> <div class="sent_msg"> <p>' + value.message_from_operator + '</p><span class="time_date"> ' + value.message_datetime + ' </span></div></div></div>');
                                    }
                                    if (value.message_attachment != null && value.message_attachment != "") {
                                        container.append('<div class="outgoing_msg"> <div class="sent_msg"> <a href="' + '{{asset("/storage/files")}}' + '/' + value.message_attachment + '" download>' + value.message_attachment + '</a><span class="time_date"> ' + value.message_datetime + ' </span></div></div></div>');
                                    }
                                }
                            })
                            containerParent.append(container);
                            var mainContainer = containerParent.append('<div class="type_msg"><div class="input_msg_write"><input class="client_id" type="hidden" value="' + response.clientInfo.chat_id + '" /><input type="text" class="write_msg" placeholder="Type a message" /><input type="file" class="upload_file" name="file" style="display: none"><button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button><button class="file_atch_btn" type="button"><i class="fa fa-plus-square" aria-hidden="true"></i></button></div>')

                            $('.tab-pane').each(function(i, obj) {
                                if ($(this).attr('data-id') == response.clientInfo.chat_id) {
                                    $(this).html(mainContainer)
                                }
                            })

                            // createCustomHtml(response.clientInfo.chat_id, container);
                            // if ($('#v-pills-tabContent>div').attr('data-id') == response.clientInfo.chat_id) {
                            //     console.log('got')
                            //     // $('#v-pills-tabContent').html(container);
                            // }

                        }
                    }
                })
                // window.location.reload()
            }
        }
    }

    function saveDate(data) {
        $.ajax({
            method: 'POST',
            url: '{{ route("sir-chat-client") }}',
            data: {
                userType: data.userType,
                from: data.from,
                msg: data.msg,
                pharmacyId: data.pharmacyId,
                to: data.to,
                _token: '{{csrf_token()}}'
            },
            success: function(response) {
                console.log(response)
            }
        });
    }
</script>
@endsection

@section('scripts')
<script>

</script>
@endsection