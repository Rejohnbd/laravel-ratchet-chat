-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 08, 2021 at 04:06 PM
-- Server version: 5.7.24
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_lara_twilio`
--

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `chat_id` int(11) NOT NULL,
  `chat_date` date NOT NULL,
  `chat_user_full_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `chat_user_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `chat_user_phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `chat_active` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=Yes, 0=No',
  `chat_in_progress` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=Yes, 0=No',
  `chat_unread_message_count` int(11) NOT NULL,
  `chat_initiated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`chat_id`, `chat_date`, `chat_user_full_name`, `chat_user_email`, `chat_user_phone`, `chat_active`, `chat_in_progress`, `chat_unread_message_count`, `chat_initiated_at`) VALUES
(6, '2021-01-05', 'Rejohn', 'rejohn@mail.com', '12345', 1, 1, 0, '2021-01-02 08:06:45'),
(28, '2021-01-05', 'Atik', 'atik@mail.com', '1234', 1, 1, 0, '2021-01-03 20:34:52');

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`id`, `name`, `email`, `phone`, `created_at`, `updated_at`) VALUES
(27, 'aa', 'aa@mail.com', '12345687', '2020-12-09 04:33:56', '2020-12-09 04:33:56'),
(28, 'bb', 'bb@mail.com', '12345687', '2020-12-09 04:34:33', '2020-12-09 04:34:33');

-- --------------------------------------------------------

--
-- Table structure for table `chat_conversation`
--

CREATE TABLE `chat_conversation` (
  `id` int(11) NOT NULL,
  `chat_id` int(11) NOT NULL COMMENT 'FK',
  `admin_id` int(11) NOT NULL,
  `pharmacy_id` int(11) NOT NULL,
  `message_from` varchar(20) COLLATE utf8mb4_bin NOT NULL COMMENT 'user/operator',
  `message_number` int(11) NOT NULL,
  `message_from_user` text COLLATE utf8mb4_bin NOT NULL,
  `message_from_operator` text COLLATE utf8mb4_bin NOT NULL,
  `message_attachment` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `message_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `chat_conversation`
--

INSERT INTO `chat_conversation` (`id`, `chat_id`, `admin_id`, `pharmacy_id`, `message_from`, `message_number`, `message_from_user`, `message_from_operator`, `message_attachment`, `message_datetime`) VALUES
(647, 28, 1, 1, 'client', 1, 'hi', '', NULL, '2021-01-05 16:57:01'),
(648, 6, 1, 1, 'client', 1, 'hi', '', NULL, '2021-01-05 16:57:35'),
(649, 28, 1, 1, 'admin', 2, '', 'hello', NULL, '2021-01-05 16:58:35'),
(650, 28, 1, 1, 'client', 3, 'yes ia user one', '', NULL, '2021-01-05 16:58:47'),
(651, 28, 1, 1, 'admin', 4, '', '', '2021_01_05_16_58_123.txt', '2021-01-05 16:58:59'),
(652, 28, 1, 1, 'admin', 5, '', '2021_01_05_16_58_123.txt', NULL, '2021-01-05 16:59:00'),
(653, 6, 1, 1, 'client', 2, 'hi admin', '', NULL, '2021-01-05 16:59:16'),
(654, 6, 1, 1, 'admin', 3, '', 'ok get', NULL, '2021-01-05 16:59:26'),
(655, 6, 1, 1, 'client', 4, 'hi i am back', '', NULL, '2021-01-05 17:00:35'),
(656, 28, 1, 1, 'admin', 6, '', 'see my attachement', NULL, '2021-01-05 17:02:02'),
(657, 28, 1, 1, 'client', 7, 'ok see', '', NULL, '2021-01-05 17:02:12'),
(658, 6, 1, 1, 'client', 5, 'no response get', '', NULL, '2021-01-05 17:03:19'),
(659, 6, 1, 1, 'client', 6, 'hii', '', NULL, '2021-01-05 17:03:24'),
(660, 6, 1, 1, 'client', 7, 'hii', '', NULL, '2021-01-05 17:03:29'),
(661, 6, 1, 1, 'client', 8, 'qwer', '', NULL, '2021-01-05 17:03:34');

-- --------------------------------------------------------

--
-- Table structure for table `chat_message`
--

CREATE TABLE `chat_message` (
  `id` bigint(20) NOT NULL,
  `chat_user_id` bigint(20) NOT NULL,
  `admin_id` bigint(20) NOT NULL,
  `pharmacy_id` bigint(20) NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chat_message`
--

INSERT INTO `chat_message` (`id`, `chat_user_id`, `admin_id`, `pharmacy_id`, `message`, `created_at`, `updated_at`) VALUES
(7, 27, 1, 1, 'Hi, I am user 27', '2020-12-15 10:43:15', '2020-12-15 10:43:15'),
(8, 28, 1, 1, 'hi I am user 28', '2020-12-15 10:48:15', '2020-12-15 10:48:15'),
(136, 9, 1, 1, 'hi', '2021-01-02 04:58:12', '2021-01-02 04:58:12'),
(137, 9, 1, 1, 'hi', '2021-01-02 04:58:13', '2021-01-02 04:58:13'),
(138, 9, 1, 1, 'hi', '2021-01-02 04:58:13', '2021-01-02 04:58:13'),
(139, 6, 1, 1, 'hi', '2021-01-02 04:58:34', '2021-01-02 04:58:34'),
(140, 6, 1, 1, 'hi', '2021-01-02 04:58:35', '2021-01-02 04:58:35'),
(141, 6, 1, 1, 'hi', '2021-01-02 04:58:35', '2021-01-02 04:58:35'),
(142, 6, 1, 1, 'hi', '2021-01-02 06:09:24', '2021-01-02 06:09:24'),
(143, 6, 1, 1, 'hi', '2021-01-02 06:24:03', '2021-01-02 06:24:03'),
(144, 6, 1, 1, 'hi', '2021-01-02 06:24:04', '2021-01-02 06:24:04');

-- --------------------------------------------------------

--
-- Table structure for table `chat_reply`
--

CREATE TABLE `chat_reply` (
  `id` bigint(20) NOT NULL,
  `admin_id` bigint(20) NOT NULL,
  `chat_user_id` bigint(20) NOT NULL,
  `pharmacy_id` int(20) NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chat_reply`
--

INSERT INTO `chat_reply` (`id`, `admin_id`, `chat_user_id`, `pharmacy_id`, `message`, `created_at`, `updated_at`) VALUES
(3, 1, 27, 1, 'yes user 27, I am admin', '2020-12-15 10:46:55', '2020-12-15 10:46:55'),
(4, 1, 28, 1, 'yes user 28 I am pharmacy admin', '2020-12-15 10:50:07', '2020-12-15 10:50:07'),
(5, 1, 28, 1, 'bye user 28', '2020-12-15 10:50:52', '2020-12-15 10:50:52');

-- --------------------------------------------------------

--
-- Table structure for table `chat_settings`
--

CREATE TABLE `chat_settings` (
  `id` int(11) NOT NULL,
  `setting_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `setting_value` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `chat_settings`
--

INSERT INTO `chat_settings` (`id`, `setting_name`, `setting_value`) VALUES
(2, 'operator_online_offline', 'offline');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_11_18_180457_create_chats_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@mail.com', NULL, '$2y$10$CpikmV9JWEYEzE.D/ARkoOI96iSnlELxxMbhGs7olfpdudmKKgeKS', NULL, '2020-11-18 12:15:12', '2020-11-18 12:15:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`chat_id`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_conversation`
--
ALTER TABLE `chat_conversation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_message`
--
ALTER TABLE `chat_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_reply`
--
ALTER TABLE `chat_reply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat_settings`
--
ALTER TABLE `chat_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `chat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `chat_conversation`
--
ALTER TABLE `chat_conversation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=662;

--
-- AUTO_INCREMENT for table `chat_message`
--
ALTER TABLE `chat_message`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;

--
-- AUTO_INCREMENT for table `chat_reply`
--
ALTER TABLE `chat_reply`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `chat_settings`
--
ALTER TABLE `chat_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
