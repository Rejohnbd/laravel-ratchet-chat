<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::post('generate-token', [App\Http\Controllers\ChatController::class, 'generateToken'])->name('generate-token');
Route::post('chat', [App\Http\Controllers\ChatController::class, 'chat'])->name('chat');
Route::post('new-chat', [App\Http\Controllers\ChatController::class, 'newChat'])->name('new-chat');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('chat-admin', [App\Http\Controllers\ChatController::class, 'adminHome'])->name('chat-admin');
Route::get('admin-chat/{id}', [App\Http\Controllers\ChatController::class, 'adminChat'])->name('admin-chat');

Route::post('chat-sir-temp', [App\Http\Controllers\ChatController::class, 'chatSirTemp'])->name('chat-sir-temp');
Route::get('sir-admin-chat', [App\Http\Controllers\ChatController::class, 'sirAdminChat'])->name('sir-admin-chat');
Route::post('chat-file-upload', [App\Http\Controllers\ChatController::class, 'chatFileUpload'])->name('chat-file-upload');
Route::post('chat-stop', [App\Http\Controllers\ChatController::class, 'chatStop'])->name('chat-stop');
Route::post('sir-chat-client', [App\Http\Controllers\ChatController::class, 'sirChatClient'])->name('sir-chat-client');
Route::post('find-client-by-id', [App\Http\Controllers\ChatController::class, 'sirFindClient'])->name('find-client-by-id');

// Third Try
Route::post('get-allinfo', [App\Http\Controllers\ChatController::class, 'getAllChat'])->name('get-allinfo');
