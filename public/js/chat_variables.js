    var external_css = '<link rel="stylesheet" href="./css/material-design-iconic-font.min.css" />'
                       +'<link rel="stylesheet" type="text/css" href="css/main.css" />';
    var internal_css = '<style>'
            + '#end_chat {'
            + 'margin: 0px 0px 0px 147px;'
            + 'background-color: #cccccc;'
            + 'cursor: pointer;'
            + 'line-height: 25px;'
            + '}'
            + '</style>';
    var chat_body = '<div class="fabs">'
                  + '<div class="chat">'
                  + '<div class="chat_header">'
                  + '<div class="chat_option">'
                  + '<div class="header_img">'
                  + '<img src="image/admin_avatar.png"/>'
                  + '</div>'
                  + '<span id="chat_head">Support</span> <br> <span class="agent"></span> <span class="online">(Online)</span>'
                  + '<span id="chat_fullscreen_loader" class="chat_fullscreen_loader"><i class="fullscreen zmdi zmdi-window-maximize"></i></span>'
                  + '</div>'
                  + '</div>'
                  + '<div class="chat_body chat_login">'
                  + '<a id="chat_first_screen" class="fab"><i class="zmdi zmdi-arrow-right"></i></a>'
                  + '<p>Hi, your name, email or phone pls. Thanks<br>'
                  + '<input type="text" id="user_name" placeholder="Name" style="width:200px;margin: 5px"><br>'
                  + '<input type="text" id="user_email" placeholder="Email" style="width:200px;margin: 5px"><br>'
                  + '<input type="text" id="user_phone" placeholder="Phone" style="width:200px;margin: 5px">'
                  + '</p>'
                  + '</div>'
                  + '<div id="chat_converse" class="chat_conversion chat_converse">'
                  + '<span class="chat_msg_item chat_msg_item_admin">'
                  + '<div class="chat_avatar">'
                  + '<img src="image/admin_avatar.png"/>'
                  + '</div>Hey there! Any question?</span>'
                  + '</div>'
                  + '<div class="fab_field">'
                  + '<div>'
                  + '<textarea id="chat_message" name="chat_message" placeholder="Send a message" class="chat_field chat_message"></textarea>'
                  + '<a id="fab_send" class="fab"><i class="zmdi zmdi-mail-send"></i></a>'
                  + '</div>'
                  + '<div>'
                  + '<input type="file" name="file" id="file" style="display: none">'
                  + '<a id="" class="fab fab_camera attach_file" title="Attach file"><i class="zmdi zmdi-attachment"></i></a>'
                  + '<button id="end_chat">End Chat</button>'
                  + '<input type="hidden" id="chat_id" value=""/>'
                  + '<input type="hidden" id="message_number" value=""/>'
                  + '<input type="hidden" id="attachment" value="0" />'
                  + '<input type="hidden" id="attachment_no" value="1" />'
                  + '</div>'
                  + '</div>'
                  + '</div>'
                  + '<a id="prime" class="fab"><i class="prime zmdi zmdi-comment-outline"></i></a>'
                  + '</div>';